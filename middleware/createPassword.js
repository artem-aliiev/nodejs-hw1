const fs = require('fs');

const createPassword = (req, res, next) => {
    const {filename, password} = req.body;
    const passwordFilePath ='./passwords.json'

    if(!password) {
        return next();
    }

    if(!password.trim()) {
        return res.status(400).json({ 'message': 'Please enter password' });
    }

    try {
        let passwords = {};
        if(fs.existsSync(passwordFilePath)) {
            passwords = JSON.parse(fs.readFileSync(passwordFilePath, 'utf8'));
        }
        passwords[filename] = {filename: filename, password: password};
        passwords = JSON.stringify(passwords);
        fs.writeFileSync(passwordFilePath, passwords, 'utf8');
    } catch(e) {
        return res.status(500).json({ 'message': 'Server error' });
    }
    next();
}

module.exports = createPassword;