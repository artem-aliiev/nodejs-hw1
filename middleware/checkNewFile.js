const fileMask = /^[a-z0-9_.@()-]+\.(log|txt|json|yaml|xml|js)$/i;
const fs = require('fs');

const checkNewFile = (req, res, next) => {
    const {filename, content} = req.body; 
    const path = `./files/${filename}`;

    if(!content) {
        return res.status(400).json({ 'message': 'Please specify "content" parameter' });
    } 
    
    if(!filename) {
        return res.status(400).json({ 'message': 'Please specify "filename" parameter' });
    } 
    
    if(!fileMask.test(filename)) {
        return res.status(400).json({ 'message': 'Please enter valid filename' });
    }
    
    try {
        if(fs.existsSync(path)) {
            return res.status(400).json({ 'message': `File with filename "${filename}" already exist` });
        }        
    } catch(e) {
        return res.status(500).json({ 'message': 'Server error' });
    }

    next();
}

module.exports = checkNewFile;