const fs = require('fs');

const checkFolder = (req, res, next) => {
    const path = `././files/`;

    try {
        if(!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
    } catch(e) {
        return res.status(500).json({ 'message': 'Server error' });
    }

    next();
}

module.exports = checkFolder;