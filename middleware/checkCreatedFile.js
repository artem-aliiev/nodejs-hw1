const fileMask = /^[a-z0-9_.@()-]+\.(log|txt|json|yaml|xml|js)$/i;
const fs = require('fs');

const checkCreatedFile = (req, res, next) => {
    const filename = req.params.filename;
    const path = `./files/${filename}`;
    
    if(!filename) {
        return res.status(400).json({ 'message': 'Please specify "filename" parameter' });
    } 
    
    if(!fileMask.test(filename)) {
        return res.status(400).json({ 'message': 'Please enter valid filename' });
    }
    
    try {
        if(!fs.existsSync(path)) {
            return res.status(400).json({ 'message': `No file with '${filename}' filename found` });
        }
    } catch(e) {
        return res.status(500).json({ 'message': 'Server error' });
    }

    next();
}

module.exports = checkCreatedFile;