const fs = require('fs');

const checkPassword = (req, res, next) => {
    const filename = req.params.filename; 
    const password = req.query.password;
    const passwordFilePath ='./passwords.json'

    try {
        const passwords = JSON.parse(fs.readFileSync(passwordFilePath, 'utf8'));
        if(!passwords[filename]) {
            return next();
        }

        if(passwords[filename].password !== password) {
            return res.status(400).json({ 'message': 'Please enter valid password' });
        }

    } catch(e) {
        return res.status(500).json({ 'message': 'Server error' });
    }
    
    next();
}

module.exports = checkPassword;