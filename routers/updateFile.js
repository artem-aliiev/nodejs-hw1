const fs = require('fs')

const updateFile = (req, res) => {
    const {filename, content} = req.body; 
    const path = `./files/${filename}`;
    try {
        fs.writeFileSync(path, content, 'utf-8');
        return res.status(200).json({ 'message': 'File updated successfully' });
    } catch (e) {
        return res.status(500).json({ 'message': 'Server error' });
    }    
}

module.exports = updateFile;