const fs = require('fs')

const createFile = (req, res) => {
    const {filename, content} = req.body; 
    const path = `./files/${filename}`;
    try {
        fs.writeFileSync(path, content, 'utf-8');
        return res.status(200).json({ 'message': 'File created successfully' });
    } catch (e) {
        res.status(500).json({ 'message': 'Server error' });
    }
}

module.exports = createFile;