const fs = require('fs');

const getFile = (req, res) => {
    const filename = req.params.filename; 
    const path = `./files/${filename}`;

    let fileData = {};
    fileData.message = 'Success';
    fileData.filename = filename;
    
    try {
        fileData.content = fs.readFileSync(path, 'utf8');
        fileData.extension = filename.substr(filename.lastIndexOf('.') + 1);
        fileData.uploadedDate = fs.statSync(path, 'utf8').birthtime;
    } catch (e) {
        return res.status(500).json({ 'message': 'Server error' });
    }
    return res.status(200).json(fileData);
}

module.exports = getFile;