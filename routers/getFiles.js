const fs = require('fs');

const getFiles = (req, res) => {
    const path = `./files`;
    try {
        fs.readdir(path, (err, files) => {
            if(err) {
                return res.status(500).json({ 'message': 'Server error' });
            } else {
                return res.status(200).json({ 'message': 'Success', 'files': files });
            }
          });
    } catch (e) {
        return res.status(500).json({ 'message': 'Server error' });
    }
}

module.exports = getFiles;