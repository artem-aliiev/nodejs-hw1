const express = require('express');
const app = express();

const createFile = require('./routers/createFile');
const getFiles = require('./routers/getFiles');
const getFile = require('./routers/getFile');
const updateFile = require('./routers/updateFile');
const deleteFile = require('./routers/deleteFile');
const checkNewFile = require('./middleware/checkNewFile');
const checkCreatedFile = require('./middleware/checkCreatedFile');
const createPassword = require('./middleware/createPassword');
const checkPassword = require('./middleware/checkPassword');
const checkFolder = require('./middleware/checkFolder');

const port = '8080';


app.use(express.json());
app.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
});

app.post('/api/files', checkFolder, checkNewFile, createPassword, createFile);
app.get('/api/files', checkFolder, getFiles);
app.get('/api/files/:filename', checkCreatedFile, checkPassword, getFile);
app.put('/api/files/:filename', checkCreatedFile, checkPassword, updateFile);
app.delete('/api/files/:filename', checkCreatedFile, checkPassword, deleteFile);

app.use((req, res) => {
    res.status(404).json({ message: 'Client error' });
});

app.listen(port, () => {
    console.log(`Server works at port ${port}`);
})